
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
	public static void main(String[] args) throws IOException {
		
		InputStreamReader leitorEntrada = new InputStreamReader(System.in);
		BufferedReader leitor = new BufferedReader(leitorEntrada);
		
		
		System.out.println("Digite qual conversão deseja:");
		System.out.println("1- Celsius para Fahrenheit");
		System.out.println("2- Fahrenheit para Celsius");
		System.out.println("3- Celsius para Kelvin");
		System.out.println("4- Kelvin para Celsius");
		System.out.println("5- Fahrenheit para Kelvin");
		System.out.println("6- Kelvin para Fahrenheit");
		
		
		int opcaoLida = Integer.parseInt(leitor.readLine());
		
		
		System.out.println("Digite o valor que deseja converter:");
		
		
		double valorDigitado = Double.parseDouble(leitor.readLine());
		
		
		Conversor meuConversor = new Conversor();
		
		
		double resultadoConversao = 0;
		
		
		switch (opcaoLida){
			case 1:
				resultadoConversao = meuConversor.celsiusParaFahrenheit(valorDigitado);
				System.out.println(valorDigitado + " graus celsius convertido é igual a " + resultadoConversao + " graus fahrenheit.");
				break;
			case 2:
				resultadoConversao = meuConversor.fahrenheitParaCelsius(valorDigitado);
				System.out.println(valorDigitado + " graus fahrenheit convertido é igual a " + resultadoConversao + " graus celsius.");
				break;
			case 3:
				resultadoConversao = meuConversor.celsiusParaKelvin(valorDigitado);
				System.out.println(valorDigitado + " graus celsius convertido é igual a " + resultadoConversao + " graus kelvin.");
				break;
			case 4:
				resultadoConversao = meuConversor.kelvinParaCelsius(valorDigitado);
				System.out.println(valorDigitado + " graus kelvin convertido é igual a " + resultadoConversao + " graus celsius.");
				break;
			case 5:
				resultadoConversao = meuConversor.fahrenheitParaKelvin(valorDigitado);
				System.out.println(valorDigitado + " graus fahrenheit convertido é igual a " + resultadoConversao + " graus kelvin.");
				break;
			case 6:
				resultadoConversao = meuConversor.kelvinParaFahrenheit(valorDigitado);
				System.out.println(valorDigitado + " graus kelvin convertido é igual a " + resultadoConversao + " graus fahrenheit.");
				break;
			default:
				System.out.println("Opção inválida");
		}
	}
}